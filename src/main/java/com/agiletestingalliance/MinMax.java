package com.agiletestingalliance;

public class MinMax {

	public int functio(int alpha, int beta) {
		return Math.max(alpha, beta); 
	}

	
	public String bar(String string) {
		if (string != null && !string.equals("")) {
			return string;
		}
		return null; // or return an appropriate default value
	}
	

}
